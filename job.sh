#!/bin/bash -l
# Standard output and error:
##SBATCH -o ./job.out.%j
##SBATCH -e ./job.err.%j
# Initial working directory:
#SBATCH -D ./
# Job name
#SBATCH -J cusolvermp_reproducer

#SBATCH --nodes=1
#SBATCH --constraint="gpu"

## comment the line below out if gpudev partition is heavily occupied
#SBATCH --partition=gpudev

#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=18

#SBATCH --gres=gpu:a100:4
##SBATCH --mem=125000

#SBATCH --mail-type=all
#SBATCH --time=00:02:00

set -v # verbose

echo nodes: $SLURM_JOB_NODELIST

source build.sh
export UCC_CONFIG_FILE=$NVHPC_ROOT/math_libs/11.8/share/ucc.conf

export LD_LIBRARY_PATH=$NVHPC_ROOT/math_libs/lib64:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=$NVHPC_ROOT/comm_libs/nvshmem/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=$NVHPC_ROOT/comm_libs/nccl/lib:${LD_LIBRARY_PATH}

#__________________________________________________________________________

# This fails
export LD_LIBRARY_PATH=/mpcdf/soft/SLE_15/packages.norpm/x86_64/openucx/ucc-1.3.0_cuda_11_ucx-1.16/lib:${LD_LIBRARY_PATH}

# This works
#export LD_LIBRARY_PATH=${NVHPC_ROOT}/comm_libs/11.8/hpcx/latest/ucc/lib:${LD_LIBRARY_PATH}

#__________________________________________________________________________

srun ./mp_syevd

