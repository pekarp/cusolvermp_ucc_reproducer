# cusolverMP + UCC reproducer



<!-- ## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.mpcdf.mpg.de/pekarp/cusolvermp_ucc_reproducer.git
git branch -M main
git push -uf origin main
``` -->

## UCC

UCC was installed by Tobias in the following way:

```
#!/bin/bash

wget https://github.com/openucx/ucc/archive/refs/tags/v1.3.0.tar.gz
tar -xzvf v1.3.0.tar.gz
cd ucc-1.3.0/

module purge
module load gcc/11
module load cuda/11.6

./autogen.sh
./configure --prefix=/mpcdf/soft/SLE_15/packages.norpm/x86_64/openucx/ucc-1.3.0_cuda_11_ucx-1.16 --with-ucx=/mpcdf/soft/SLE_15/packages.norpm/x86_64/openucx/ucx-1.16.0_cuda_11_gdr_2.4_2024-06-12
make -j 6
```

## cusolverMP

Here is the reproducer for cusolverMP (adapted from [here](https://github.com/NVIDIA/CUDALibrarySamples/tree/master/cuSOLVERMp)):

```
git clone https://gitlab.mpcdf.mpg.de/pekarp/cusolvermp_ucc_reproducer.git
cd cusolvermp_ucc_reproducer
bash build.sh
sbatch job.sh
```
Here is a part of error message:
```
[1721923699.823497] [ravg1164:57080:0]          ucc_mc.c:142  UCC  ERROR no components supported memory type cuda available
[ravg1164:57080:0:57080] Caught signal 11 (Segmentation fault: address not mapped to object at address 0x8)
[1721923695.150348] [ravg1164:57081:0]           ib_md.c:1105 UCX  WARN  mlx5_1: relaxed order memory access requested, but unsupported
[1721923695.403349] [ravg1164:57081:0]           ib_md.c:1105 UCX  WARN  mlx5_1: relaxed order memory access requested, but unsupported
[1721923696.037756] [ravg1164:57081:0]           ib_md.c:1105 UCX  WARN  mlx5_1: relaxed order memory access requested, but unsupported
[1721923696.110248] [ravg1164:57081:0]           ib_md.c:1105 UCX  WARN  mlx5_1: relaxed order memory access requested, but unsupported
[1721923699.872947] [ravg1164:57081:0]          ucc_mc.c:142  UCC  ERROR no components supported memory type cuda available
[ravg1164:57081:0:57081] Caught signal 11 (Segmentation fault: address not mapped to object at address 0x8)
==== backtrace (tid:  57081) ====
 0 0x0000000000016910 __funlockfile()  ???:0
 1 0x00000000000291db ucc_tl_ucp_allreduce_knomial_init_common()  /u/tmelson/Code/ucc/ucc-1.3.0/src/components/tl/ucp/allreduce/allreduce_knomial.c:245
 2 0x00000000000184a8 ucc_coll_init()  /u/tmelson/Code/ucc/ucc-1.3.0/src/coll_score/ucc_coll_score_map.c:130
 3 0x0000000000010138 ucc_collective_init()  /u/tmelson/Code/ucc/ucc-1.3.0/src/core/ucc_coll.c:234
 4 0x000000000009d6d4 UCCCollImpl::allreduce()  ???:0
 5 0x000000000006bc58 cal_allreduce()  ???:0
 6 0x0000000000209c61 mp_sytrd<double, double>()  ???:0
 7 0x0000000000240c6c mp_syevd<double, double>()  ???:0
 8 0x000000000023bace cusolverMpSyevd()  ???:0
 9 0x0000000000403738 main()  /raven/u/pekarp/git/reproducers/cusolvermp_ucc_reproducer/mp_syevd.c:434
10 0x000000000003524d __libc_start_main()  ???:0
11 0x0000000000401dda _start()  /home/abuild/rpmbuild/BUILD/glibc-2.31/csu/../sysdeps/x86_64/start.S:120
=================================
```